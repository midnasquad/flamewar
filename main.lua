--[[
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]--

local cron = require("cron")

-- Internal game resolution
WIDTH = 1920
HEIGHT = 1080
-- Windowed mode
W_WIDTH = 896
W_HEIGHT = 504

timers = {}

screen = 'titleScreen'
round = 0
players = {}
btn_chain = {}
round_winners = {}
news_feed = {}
over = false
winner = ""
level = 1
announcement = ""
spamjam = false
spam_btn = ""
jam_winner = ""


-- Initializes n players
function createPlayers(n)
  players = {}
  for i = 1, n, 1 do
    table.insert(
      players,
      -- TODO: This is better as a metatable, but I'm a novice.
      --       https://www.lua.org/pil/16.html
      {id=i, joystick=nil, input={}, score=0, combo=0, status="GO!", blocked=false, jam_score=0}
    )
  end
end

function love.load()
  math.randomseed(os.time()) -- needed to generate random buttons
  instructions = love.graphics.newImage("data/instructions.png")
  title = love.graphics.newImage("data/title.png")
  bg = love.graphics.newImage("data/bg.png")
  announcement_block = love.graphics.newImage("data/announcement-block.png")
  btn_sprites = {
    a = love.graphics.newImage("data/btn_a.png"),
    b = love.graphics.newImage("data/btn_b.png"),
    x = love.graphics.newImage("data/btn_x.png"),
    y = love.graphics.newImage("data/btn_y.png"),
    a_gray = love.graphics.newImage("data/btn_a_gray.png"),
    b_gray = love.graphics.newImage("data/btn_b_gray.png"),
    x_gray = love.graphics.newImage("data/btn_x_gray.png"),
    y_gray = love.graphics.newImage("data/btn_y_gray.png"),
  }
  overlay_failed_btn = love.graphics.newImage("data/overlay_failed_btn.png")
  score_font = love.graphics.newFont("data/BungeeInline-Regular.ttf", 180)
  announcement_font = love.graphics.newFont("data/BungeeInline-Regular.ttf", 100)
  status_font = love.graphics.newFont("data/BioRhymeExpanded-Regular.ttf", 45)
  msg_sprites = {
    love.graphics.newImage("data/p1_msg.png"),
    love.graphics.newImage("data/p2_msg.png"),
    love.graphics.newImage("data/p1_msg_supporter.png"),
    love.graphics.newImage("data/p2_msg_supporter.png"),
  }
  p1_echo = love.graphics.newImage("data/echo1.png")
  p2_echo = love.graphics.newImage("data/echo2.png")
  title_music = love.audio.newSource("data/music/title_theme.ogg", "stream")
  title_music:setLooping(true)
  title_music:play()
  gameplay_music = love.audio.newSource("data/music/gameplay.ogg", "stream")
  gameplay_music:setLooping(true)
  sfx_combo = love.audio.newSource("data/sfx/combo.ogg", "static")
  sfx_correct = love.audio.newSource("data/sfx/correct.ogg", "static")
  sfx_spamjam = love.audio.newSource("data/sfx/spamjam.ogg", "static")
  sfx_win = love.audio.newSource("data/sfx/win.ogg", "static")
  sfx_wrong = love.audio.newSource("data/sfx/wrong.ogg", "static")
  the_lick = love.audio.newSource("data/sfx/lick.ogg", "static")
  sfx_volume = 0.3
  sfx_combo:setVolume(sfx_volume)
  sfx_correct:setVolume(sfx_volume)
  sfx_spamjam:setVolume(sfx_volume)
  sfx_win:setVolume(sfx_volume)
  sfx_wrong:setVolume(sfx_volume)
  createPlayers(2)
end

function love.update(dt)
  -- Update any timers
  for key, timer in pairs(timers) do
    if timer ~= nil then
      timer:update(dt)
    end
  end
end

function love.draw()
  zoom()

  if screen == 'titleScreen' then
    love.graphics.draw(title)
  elseif screen == 'instructionScreen' then
    love.graphics.draw(instructions)
  elseif screen == 'gamePlay' then
    love.graphics.draw(bg)
    love.graphics.setFont(status_font)
    love.graphics.setColor(0, 0, 0)
    local p1_center_offset = string.len(players[1]["status"]) * 10
    local p2_center_offset = string.len(players[2]["status"]) * 10
    love.graphics.print(players[1]["status"], 160 - p1_center_offset, 650)
    love.graphics.print(players[2]["status"], 1560 - p2_center_offset, 650)
    love.graphics.setColor(1, 1, 1)

    -- Draw messages
    local msg_offset = math.max(0, #news_feed - 9) -- show only 9 at once
    for i = 1 + msg_offset, #news_feed do
      local p = news_feed[i]
      local sprite = msg_sprites[p]
      local d = #news_feed - i
      love.graphics.draw(sprite, 630, 936 - 100 * d)
    end

    if spamjam == false then
      -- On-screen buttons
      for n, btn in pairs(btn_chain) do
        -- zero-indexed loop
        local i = n - 1
        -- Row we're on. 5 buttons per row.
        local row = math.floor(i / 5)
        -- Button diameter = 80px + 14px padding
        local bd = 94
        -- Set coordinates
        local x = (i % 5 * bd) + 35
        local y = row * bd + 340

        -- Determine button onscreen display state
        local p1_btn, p1_fail = getBtnSprite(players[1], btn, n)
        local p2_btn, p2_fail = getBtnSprite(players[2], btn, n)

        -- Draw the button
          love.graphics.draw(p1_btn, x, y) -- P1
          love.graphics.draw(p2_btn, x+1395, y) -- P2

        -- Time in seconds to block a player for a wrong input
        local block_time = 2

        -- Draw the X over the button if the player failed
        if p1_fail then
          love.graphics.draw(overlay_failed_btn, x-17, y-8)
          players[1]["status"] = "MISS!"
          players[1]["combo"] = 0
          -- Block the player for block_time seconds
          if timers['p1_x'] == nil then
            freeze(players[1])
            print("Player 1 frozen.")
            timers['p1_x'] = cron.after(block_time, function ()
              timers['p1_x'] = nil
              playerReset(players[1])
              unfreeze(players[1])
            end)
          end
        end
        if p2_fail then
          love.graphics.draw(overlay_failed_btn, x-17+1395, y-8)
          players[2]["status"] = "MISS!"
          players[2]["combo"] = 0
          -- Block the player for block_time seconds
          if timers['p2_x'] == nil then
            freeze(players[2])
            print("Player 2 frozen.")
            timers['p2_x'] = cron.after(block_time, function ()
              timers['p2_x'] = nil
              playerReset(players[2])
              unfreeze(players[2])
            end)
          end
        end
      end
    end

    -- Advance the round when there's a winner
    local winner = roundWinner()
    if winner then
      print("Player " .. winner["id"] .. " won this round!")
      table.insert(round_winners, winner["id"])
      table.insert(news_feed, winner["id"])
      winner["score"] = winner["score"] + level
      winner["combo"] = winner["combo"] + 1
      if winner["combo"] % 3 == 0 then
        getSupport(winner)
      end
      advanceRound()
      print("Round " .. round .. " started!")
    end

  end

  -- Draw the scores
  if screen == 'gamePlay' or screen == 'instructionScreen' then
    love.graphics.setFont(score_font)
    love.graphics.setColor(157/255, 0, 218/255)
    love.graphics.print(players[1]["score"], 150, 775)
    love.graphics.setColor(1, 96/255, 0)
    love.graphics.print(players[2]["score"], 1550, 775)
    love.graphics.setColor(1, 1, 1)
  end

  -- Announce levels and Echo Chamber
  function levelUp()
    love.graphics.setFont(announcement_font)
    love.graphics.setColor(59/255, 89/255, 152/255)
    love.graphics.print(announcement, 720, 300)
    love.graphics.setColor(1, 1, 1)
  end

  if announcement == "1 echo" and screen == 'gamePlay' then
    love.graphics.draw(p1_echo)
  elseif announcement == "2 echo" and screen == 'gamePlay' then
    love.graphics.draw(p2_echo)
  elseif announcement ~= "" and screen == 'gamePlay' then
    levelUp()
  end

  -- SpamJam screen
  if screen == 'gamePlay' and spamjam == true then
    -- draw Spam Jam title and block
    love.graphics.draw(announcement_block)
    love.graphics.setFont(announcement_font)
    love.graphics.setColor(255/255, 0, 0)
    love.graphics.print("SPAM JAM!", 670, 200)
    love.graphics.setColor(1, 1, 1)
    love.graphics.setFont(status_font)
    love.graphics.setColor(0, 0, 0)
    love.graphics.print("spam that button!", 600, 800)
    love.graphics.setColor(1, 1, 1)
    -- draw spam scores
    love.graphics.setFont(score_font)
    love.graphics.setColor(157/255, 0, 218/255)
    love.graphics.print(players[1]["jam_score"], 635, 600)
    love.graphics.setColor(1, 96/255, 0)
    love.graphics.print(players[2]["jam_score"], 1070, 600)
    love.graphics.setColor(1, 1, 1)
    -- draw button
    love.graphics.draw(btn_sprites[spam_btn], 850, 350, 0, 3, 3)
  end

  -- bring up Win Screen if game is won
  if over == true then
    love.graphics.draw(announcement_block)
    print(winner)
    if winner == "Player 1" then
      love.graphics.setColor(157/255, 0, 218/255)
    else
      love.graphics.setColor(1, 96/255, 0)
    end
    love.graphics.setFont(announcement_font)
    love.graphics.print(winner, 720, 300)
    love.graphics.print("Wins!", 800, 400)
    love.graphics.setColor(59/255, 89/255, 152/255)
    love.graphics.print("REPLAY", 770, 650)
    love.graphics.setFont(status_font)
    love.graphics.print("press start", 750, 750)
    love.graphics.setColor(1, 1, 1)
  end
end

-- Returns the button sprite for the given player and button position
function getBtnSprite(player, btn, n)
  local sprite, fail
  if #player["input"] < n then
    sprite = btn_sprites[btn]
  elseif player["input"][n] == btn then
    sprite = btn_sprites[btn.."_gray"]
  else
    sprite = btn_sprites[btn]
    fail = true
  end
  return sprite, fail
end

-- Scales the graphics for fullscreen vs windowed mode
function zoom()
  local width, height

  -- Windowed mode
  if love.window.getFullscreen() == false then
    width = W_WIDTH

  -- Scale for fullscreen
  else
    local _, _, flags = love.window.getMode()
    width, height = love.window.getDesktopDimensions(flags.display)
  end

  -- FIXME: this only works if the window is smaller than the screen and the
  --        same aspect ratio as the screen
  local scale = width / WIDTH
  love.graphics.scale(scale, scale)
end

function love.keypressed(key, scancode, isrepeat)
  -- Fullscreen hotkey
  if key == "f11" then
    if love.window.getFullscreen() == false then
      love.window.setFullscreen(true)
    else
      love.window.setMode(W_WIDTH, W_HEIGHT)
    end
  end
  -- Use escape to exit fullscreen
  if key == "escape" then
    if love.window.getFullscreen() then
      love.window.setMode(W_WIDTH, W_HEIGHT)
    end
  end
end

function love.gamepadpressed(joystick, button)
  print(joystick, button)
  if spamjam == false then
    local p = getPlayer(joystick)
    if screen == 'titleScreen' and button == 'start' then
      screen = 'instructionScreen'
      return
    end
    if screen == 'instructionScreen' and button == 'start' then
      screen = 'gamePlay'
      title_music:stop()
      gameplay_music:play()
      advanceRound()
      return
    end
    if round < 1 then
      -- Only proceed for round 1
      return
    end
    if p["blocked"] == true then
      -- Reject the button presses of blocked players!
      return
    end
    if over == true and button == 'start' then
      replay()
    elseif over == true then
      return
    end
  end
  -- Only handle valid face buttons
  local buttons = {'a', 'b', 'x', 'y'}
  local valid_input = false
  for i, btn in pairs(buttons) do
    if button == btn then
      valid_input = true
      break
    end
  end
  if not valid_input then
    return
  end

  -- Save this button input in the player's table
  if spamjam == false then
    local p = getPlayer(joystick)
    if #p["input"] < #btn_chain then
      table.insert(p["input"], button)
      local z = #p["input"]
      if p["input"][z] == btn_chain[z] then
        local sfx = love.audio.newSource("data/sfx/correct.ogg", "static")
        sfx:setVolume(sfx_volume)
        love.audio.play(sfx)
      else
        love.audio.play(sfx_wrong)
      end
    end
  end

  if spamjam == true then
    love.audio.pause(gameplay_music)
    love.audio.play(sfx_spamjam)

    print("button is: " .. button)
    if button == spam_btn then
      local p = getPlayer(joystick)
      p["jam_score"] = p["jam_score"] + 1
      print("player jam score: " .. p["jam_score"])
    end
  end


end

function love.gamepadaxis(joystick, axis, value)
  print(joystick, axis, value)
end

function love.joystickadded(joystick)
  print(joystick:getName(), " added!")
  local joysticks = love.joystick.getJoysticks()
  players[1]["joystick"] = joysticks[1]
  players[2]["joystick"] = joysticks[2]
end

-- Compares two button chains for equality
function btnChainsEqual(a, b)
  if #a ~= #b then return false end
  for i = 1, #a do
    if a[i] ~= b[i] then
      return false
    end
  end
  return true
end

-- Get the winner of this round, or nil if the round hasn't been won
function roundWinner()
  for i = 1, #players do
    if btnChainsEqual(players[i]["input"], btn_chain) and #btn_chain > 0 then
      return players[i]
    end
  end
  return nil
end

function advanceRound()
  if round > 2 then
    rollSpamJam()
  end
  if round == 0 then
    round = round + 1
    btn_chain = generateInputChain(5)
    return
  end
  resetAll()
  freezeAll()
  love.audio.play(sfx_win)
  btn_chain = {}


  function progress(s)
    timers["round"] = cron.after(s, function()
      round = round + 1
      if level >= 5 then
        btn_chain = generateInputChain(15)
      elseif level >= 3 then
        btn_chain = generateInputChain(10)
      else
        btn_chain = generateInputChain(5)
      end
      unfreezeAll()
      announcement = ""
      if spamjam == true then
        love.audio.pause(sfx_spamjam)
        spamJamWinner()
        print("Jam Winner is: " .. tostring(jam_winner))
        spamjam = false
        for i = 1, #players do
          if jam_winner == players[i] then
            announcement = "P #" .. i .." Jams!"
            players[i]["score"] = players[i]["score"] + 5
          end
          players[i]["jam_score"] = 0
          love.audio.play(gameplay_music)
        end
      end
    end)
  end

  if spamjam == true then
    progress(9)
  else
    progress(0.7)
  end

  function spamJamWinner()
    if players[1]["jam_score"] > players[2]["jam_score"] then
      jam_winner = players[1]
      print("test")
    elseif players[2]["jam_score"] > players[1]["jam_score"] then
      jam_winner = players[2]
    else
      jam_winner = "none"
    end
  end

  checkLevel()
  checkWin()
end

-- Clears some data about a player
function playerReset(player)
  player["input"] = {}
  if player["combo"] >= 3 then
    player["status"] = "+" .. player["combo"] .. " combo!"
  else
    player["status"] = "GO!"
  end
end

-- Clears some data about all players
function resetAll()
  for i = 1, #players do
    playerReset(players[i])
  end
end

-- Generates a button sequence for the player to press
-- ex: generateInputChain(5) --> {'x', 'a', 'b', 'y', 'b'}
function generateInputChain(num)
  local buttons = {'a', 'b', 'x', 'y'}
  local result = {}
  for i = 1, num, 1 do
    local x = math.ceil(math.random()*#buttons)
    local btn = buttons[x]
    table.insert(result, btn)
  end
  return result
end

-- Returns a player for the given joystick
function getPlayer(joystick)
  for i = 1, #players do
    if joystick == players[i]["joystick"] then
      return players[i]
    end
  end
end

-- Freeze all players
function freezeAll()
  for i = 1, #players do
    freeze(players[i])
  end
end

-- Unfreeze all players
function unfreezeAll()
  for i = 1, #players do
    unfreeze(players[i])
  end
end

-- Freeze a player
function freeze(player)
  player["blocked"] = true
  print("Player " .. player["id"] .. " frozen.")
end

-- Unfreeze a player
function unfreeze(player)
  player["blocked"] = false
  print("Player " .. player["id"] .. " unfrozen.")
end

-- Sends a support message for every 3rd combo
function getSupport(winner)
  print("You got support! Nice!")
  winner["status"] = "+" .. winner["combo"] .. "combo!"
  for i = 1, level do
    table.insert(news_feed, winner["id"] + 2)
  end
  winner["score"] = winner["score"] + level
  announcement = winner["id"] .. " echo"
  print(announcement)
  print(news_feed)
  print(winner["status"])
end

-- if last 9 messages = 1 and 3 only, P1 wins. if 2 and 4 only, P2 wins.
function checkWin()
  local current_board = {}
  local msg_offset = math.max(0, #news_feed - 9) -- show only 9 at once
  for i = 1 + msg_offset, #news_feed do
    local p = news_feed[i]
    table.insert(current_board, p)
  end
  -- print("original board")
  for i = 1, #current_board do
    -- print(current_board[i])
  end
  -- print("board rewritten")
  for i = 1, #current_board do
    if current_board[i] == 3 then
      current_board[i] = 1
    elseif current_board[i] == 4 then
      current_board[i] = 2
    end
    -- print(current_board[i])
  end

  -- check if P1 wins
  local p1_count = 0
  if #news_feed >= 9 then
    for i = 1, #current_board do
      if current_board[i] ~= 1 then
        p1_count = p1_count + 1
      end
    end
    if p1_count == 0 then
      announceWinner("Player 1")
    end
  end

  if players[1]["score"] >= 100 then
    announceWinner("Player 1")
  end

  -- check if P2 wins
  local p2_count = 0
  if #news_feed >= 9 then
    for i = 1, #current_board do
      if current_board[i] ~= 2 then
        p2_count = p2_count + 1
      end
    end
    if p2_count == 0 then
      announceWinner("Player 2")
    end
  end

  if players[2]["score"] >= 100 then
    announceWinner("Player 2")
  end

end


function announceWinner(player)
  spamjam = false
  love.audio.stop()
  the_lick:play()
  print(player .. " wins!")
  over = true
  winner = player
end

-- resets all tables and variables except the Players
function replay()
  print("replay!")
  screen = 'titleScreen'
  round = 0
  btn_chain = {}
  round_winners = {}
  news_feed = {}
  over = false
  winner = ""
  level = 1
  gameplay_music:stop()
  the_lick:stop()
  title_music:play()
  for i = 1, #players do
    players[i]["score"] = 0
    players[i]["combo"] = 0
    players[i]["input"] = {}
    players[i]["status"] = "GO!"
    players[i]["blocked"] = false
  end
end

-- Uses player scores to determine if it's time to level up
function checkLevel()
  for i = 1, #players do
    if players[i]["score"] >= 75 and level == 4 then
      level = 5
      announcement = "Level 5!"
    elseif players[i]["score"] >= 50 and level == 3 then
      level = 4
      announcement = "Level 4!"
    elseif players[i]["score"] >= 25 and level == 2 then
      level = 3
      announcement = "Level 3!"
    elseif players[i]["score"] >= 10 and level == 1 then
      level = 2
      announcement = "Level 2!"
    end
  end
  print("level is " .. level)
  print(announcement)
end

function rollSpamJam()
  local x = math.random(10)
  print("Spam Roll is:" .. x)
  if x == 1 then
    spamjam = true
    spam_btn_chain = generateInputChain(1)
    spam_btn = spam_btn_chain[1]
    print("SPAM JAM TIME!")
    print("Press " .. spam_btn)
    for i = 1, #players do
      players[i]["status"] = ""
    end
  end
end
