-- https://love2d.org/wiki/Config_Files
function love.conf(t)
  t.identity = "flamewars"             -- The name of the save directory (string)
  t.version = "11.1"                  -- The LÖVE version this game was made for (string)
  t.window.title = "Flame Wars - #openjam2018" -- The window title (string)
  t.window.width = 896                -- The window width (number)
  t.window.height = 504               -- The window height (number)
end
