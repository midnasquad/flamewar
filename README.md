![TITLE](https://img.itch.zone/aW1hZ2UvMzE0NjQ0LzE1NDc4MzcucG5n/original/jtZdbC.png)

# Flame Wars

This is a game created for [#openjam2018](https://itch.io/jam/open-jam-2018/community) where players press buttons to argue on the internet.

View and download the game on [Itch](https://midnasquad.itch.io/flamewars)!


# Running the game

Flame Wars is built with [love](https://love2d.org/).
You'll need to install this to run it.
In Ubuntu, use these commands to install love:
```
sudo add-apt-repository ppa:bartbes/love-stable
sudo apt update
sudo apt install -y love
```

To run the game, clone the repo, then run love in it.
```
git clone git@gitlab.com:midnasquad/flamewar.git
cd flamewar
love .

```

![PLAY](https://img.itch.zone/aW1hZ2UvMzE0NjQ0LzE1NDc4MzQucG5n/original/07g9p3.png)


# License

![GPL](https://www.gnu.org/graphics/gplv3-127x51.png)

Flame Wars is Free Software, licensed under GPL-3.0.
See the `LICENSE file` included in this repo for a copy of the full license.

Other assets including music and graphics are licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/).

Exceptions:
* `data/BioRhymeExpanded-Regular` and `data/BungeeInline-Regular.ttf` are licensed under [OFL-1.1](https://scripts.sil.org/cms/scripts/page.php?item_id=OFL_web).
* `cron.lua` is licensed under [MIT](https://opensource.org/licenses/MIT).
